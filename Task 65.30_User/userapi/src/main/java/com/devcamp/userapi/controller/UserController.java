package com.devcamp.userapi.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.userapi.model.CUser;
import com.devcamp.userapi.service.UserService;


@CrossOrigin
@RestController
@RequestMapping("/")
public class UserController {
    @Autowired
    UserService pUserService;
       // Lấy danh sách voucher dùng service.
       @GetMapping("/all-user")// Dùng phương thức GET
       public ResponseEntity<List<CUser>> getAllUserApi() {
           try {
               return new ResponseEntity<>(pUserService.getUserList(),HttpStatus.OK);            
           } catch (Exception e) {
               return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);   
           }
       }	
}
