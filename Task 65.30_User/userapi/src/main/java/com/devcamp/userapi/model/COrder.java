package com.devcamp.userapi.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name = "pizza_order")
public class COrder {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "order_code", nullable = false, updatable = false)
    private String orderCode;

    @Column(name = "pizza_size", nullable = false, updatable = true)
    private String pizzaSize;

    @Column(name = "pizza_type", nullable = false, updatable = true)
    private String pizzaType;

    @Column(name = "voucher_code", nullable = true, updatable = true)
    private String voucherCode;

    @Column(name = "price", nullable = true, updatable = true)
    private long price;

    @Column(name = "paid", nullable = true, updatable = true)
    private long paid;

    @Temporal(TemporalType.TIMESTAMP)
    @CreatedDate
    @Column(name = "created_at", nullable = true, updatable = false)
    private Date created;


    @Temporal(TemporalType.TIMESTAMP)
    @LastModifiedDate
    @Column(name = "updated_at", nullable = true, updatable = true)
    private Date updated;

    @ManyToOne
    @JoinColumn(name = "user_id", referencedColumnName = "id")
    @JsonBackReference
    private CUser cUser;

    public COrder() {
    }


    public COrder(long id, String orderCode, String pizzaSize, String pizzaType, String voucherCode, long price,
            long paid, Date created, Date updated) {
        this.id = id;
        this.orderCode = orderCode;
        this.pizzaSize = pizzaSize;
        this.pizzaType = pizzaType;
        this.voucherCode = voucherCode;
        this.price = price;
        this.paid = paid;
        this.created = created;
        this.updated = updated;
    }


    public long getId() {
        return id;
    }


    public void setId(long id) {
        this.id = id;
    }


    public String getOrderCode() {
        return orderCode;
    }


    public void setOrderCode(String orderCode) {
        this.orderCode = orderCode;
    }


    public String getPizzaSize() {
        return pizzaSize;
    }


    public void setPizzaSize(String pizzaSize) {
        this.pizzaSize = pizzaSize;
    }


    public String getPizzaType() {
        return pizzaType;
    }


    public void setPizzaType(String pizzaType) {
        this.pizzaType = pizzaType;
    }


    public String getVoucherCode() {
        return voucherCode;
    }


    public void setVoucherCode(String voucherCode) {
        this.voucherCode = voucherCode;
    }


    public long getPrice() {
        return price;
    }


    public void setPrice(long price) {
        this.price = price;
    }


    public long getPaid() {
        return paid;
    }


    public void setPaid(long paid) {
        this.paid = paid;
    }


    public Date getCreated() {
        return created;
    }


    public void setCreated(Date created) {
        this.created = created;
    }


    public Date getUpdated() {
        return updated;
    }


    public void setUpdated(Date updated) {
        this.updated = updated;
    }
    

    
    
}
