package com.devcamp.userapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.userapi.model.CUser;

public interface IUserRepository extends JpaRepository<CUser, Long>{
    
}
