package com.devcamp.userapi.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.userapi.model.CUser;
import com.devcamp.userapi.repository.IUserRepository;

@Service
public class UserService {
    @Autowired
    IUserRepository pIUserRepository;
    public ArrayList<CUser> getUserList() {
        ArrayList<CUser> listUser = new ArrayList<>();
        pIUserRepository.findAll().forEach(listUser::add);
        return listUser;
    }
}
